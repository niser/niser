/**
 * A simple ExpressJS server app
 */

'use strict'

var express = require('express'),
  http = require('http'),
  path = require('path'),
  app = express(),
  server = http.createServer(app)

app.use(express.static(path.join(__dirname, '/assets')))
app.use('/assets/js', express.static(path.join(__dirname, '/assets/js')))
app.use('/assets/css', express.static(path.join(__dirname, '/assets/css')))

app.all('/*', function (req, res, next) {
  res.sendFile('assets/index.html', { root: __dirname })
})

server.listen(3000, 'localhost')
server.on('listening', function () {
  console.log('Express server started on port %s at %s', server.address().port, server.address().address)
})
