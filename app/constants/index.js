import { default as urls } from './urls'

/**
 * List Application Angular constants here
 */
angular.module('constants').constant('URLS', urls)
