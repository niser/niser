'use strict'

/**
 * Url object
 * @type {Object}
 */
export default {
  // Third party API urls
  api: {
    // Niser firebase API entry point
    firebaseUrl: 'https://niser.firebaseio.com'
  },

  // Application self urls
  document: {
    // Route for home page
    index: '/',

    // Route for login page
    login: '/login',

    // Route for register page
    register: '/register',

    // Route for user's event list page
    events: {
      // Route for user's event list page
      landing: '/events',

      /** Route for an event page
       *
       * @note A prefix like 'event-' here is a must, otherwise, it gonna
       * conflict with 'events/new'
       *
       * @ref http://blog.2partsmagic.com/restful-uri-design/
       */
      event: '^/events/event{eventId}',

      // Route for user's event list page
      new: '^/events/new'

    }
  }
}
