'use strict'

/**
 * Represents an event object
 *
 * @module factories.Event
 */
module.exports = /*@ngInject*/ function Event ($firebaseObject) {
  var eventsRef = new Firebase(URLS.api.firebaseUrl + '/events/' + eventId),
    Event

  Event = $firebaseObject.$extend({})

  return new Event(eventId)
}
