'use strict'

/**
 * List Application Angular factories here
 */

angular.module('factories')

  .factory('Users', require('./users'))

  .factory('User', require('./user'))

  .factory('Events', require('./events'))

  .factory('Event', require('./event'))
