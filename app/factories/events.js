'use strict'

/**
 * Represents a list of events
 *
 * @module factories.Events
 */
module.exports = /*@ngInject*/ function Events ($firebaseArray, URLS) {
  var ref = new Firebase(URLS.api.firebaseUrl + '/events'),
    Events

  Events = $firebaseArray.$extend({
    getByUserId: function (userId) {},

    getByGroupId: function (groupId) {}
  })

  return new Events(ref)
}
