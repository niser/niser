'use strict'

/**
 * Represents user object
 * 
 * @module factories.UserFactory
 */
module.exports = /*@ngInject*/ function User ($firebaseObject, URLS) {
  //    var ref = new Firebase(URLS.api.firebaseUrl + '/users/'),
  //            User = $firebaseObject.$extend({})
  //
  //    return new User(ref)
  return {
    byId: function (id) {
      var ref = new Firebase(URLS.api.firebaseUrl + '/users/' + id),
        User = $firebaseObject.$extend({})
      return new User(ref)
    }
  }
}
