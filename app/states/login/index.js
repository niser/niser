'use strict'

/**
 * Controller of login page
 *
 * @module controllers.LoginPageCtrl
 */
export default /*@ngInject*/ function LoginPageCtrl ($scope, Auth, URLS, User) {
  $scope.login = function (user) {
    Auth.$authWithPassword({
      email: user.username,
      password: user.password
    }).then(function (data) {
      console.log(data)
    //            console.log(User.byId())
    }).catch(function (error) {})
  }

  $scope.logout = function (user) {
    Auth.$unauth()
  }
}
