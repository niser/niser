'use strict'

/**
 * Controller of register page
 *
 * @module controllers.RegisterPageCtrl
 */
export default /*@ngInject*/ function RegisterPageCtrl ($scope, Auth) {
  $scope.register = function (user) {
    Auth.$createUser({
      email: user.username,
      password: user.password
    }).then(function (userData) {
      $scope.notification = 'Thank you for register, please login with you username and password'
    }).catch(function (error) {
      if (error) {
        switch (error.code) {
          case 'EMAIL_TAKEN':
            $scope.notification = 'The new user account cannot be created because the email is already in use.'
            break
          case 'INVALID_EMAIL':
            $scope.notification = 'The specified email is not a valid email.'
            break
          default:
            $scope.notification = 'Error creating user:', error
        }
      } else {
        $scope.notification = 'Thank you for register, please login with you username and password'
      }
    })
  }

  $scope.hasNotification = function () {
    if ($scope.notification) {
      return true
    } else {
      return false
    }
  }
}
