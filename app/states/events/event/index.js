'use strict'

/**
 * Controller of event page
 *
 * @module controllers.EventPageCtrl
 */
export default /*@ngInject*/ function EventPageCtrl (eventObj) {
  this.eventObj = eventObj

  console.log(this.eventObj)
}
