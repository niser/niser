import { default as HomePageCtrl } from './home'
import { default as RegisterPageCtrl } from './register'
import { default as LoginPageCtrl } from './login'
import { default as EventPageCtrl } from './events/event'

/**
 * List Application Angular controllers here
 */

angular.module('states')

  // Controller for index page
  .controller('HomePageCtrl', HomePageCtrl)

  // Controller for register page
  .controller('RegisterPageCtrl', RegisterPageCtrl)

  // Controller for login page
  .controller('LoginPageCtrl', LoginPageCtrl)

  // Controller for event details page
  .controller('EventPageCtrl', EventPageCtrl)
