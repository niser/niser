/**
 * Main entry of nise angular app
 *
 * @see  https://blog.codecentric.de/en/2014/08/angularjs-browserify/
 */
angular = window.angular

// Initialises Application Angular modules
angular.module('config', [])
angular.module('constants', [])
angular.module('components', [])
angular.module('factories', [])
angular.module('states', [])
angular.module('utils', [])

/**
 * Include all third party libraries here, and browserify would combine them 
 * together in bundle.js
 */
angular.module('vendor', [
  'firebase',
  'foundation',
  'ui.router'
])

// Includes Appliaction Angular module implements
require('./config')
require('./constants')
require('./components')
require('./factories')
require('./states')
require('./utils')

// Bootstrap niserApp Angular Application
angular.module('niserApp', [
  'vendor',
  // constants has to be prio of config
  'constants',
  'config',
  'components',
  'factories',
  'utils',
  'states'
])
