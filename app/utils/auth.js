'use strict'

/**
 * Authentication functions across the app
 *
 * @module factories.Auth
 */
export default /*@ngInject*/ function Auth (URLS, $firebaseAuth) {
  var fbUrl = URLS.api.firebaseUrl,
    firebaseObj = new Firebase(fbUrl),
    auth = $firebaseAuth(firebaseObj),
    loginedUser

  return auth
}
