import { default as lodash } from './lodash'
import { default as Auth } from './auth'

/**
 * List Application Angular factories here
 */

angular.module('utils')

  .factory('_', lodash)

  .factory('Auth', Auth)
