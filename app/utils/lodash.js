'use strict'

/**
 * Represents an event object
 *
 * @module factories._
 */
export default /*@ngInject*/ function _ ($window) {
  return $window._
}
