'use strict'

/**
 * Config Application routes
 *
 */
export default /*@ngInject*/ function routeConfig (
  $locationProvider,
  $stateProvider,
  $urlRouterProvider,
  URLS
) {
  var stateUrl = URLS.document

  // Enable html5 mode to remove # in url
  $locationProvider.html5Mode(true)

  // Main app routes
  $stateProvider

    .state('register', {
      url: stateUrl.register,
      templateUrl: 'states/register/register.html',
      controller: 'RegisterPageCtrl'
    })

    .state('login', {
      url: stateUrl.login,
      templateUrl: 'states/login/login.html',
      controller: 'LoginPageCtrl'
    })

    .state('app', {
      abstract: true,
      url: stateUrl.index,
      templateUrl: 'layouts/default.html'
    })

    .state('app.home', {
      url: '',
      views: {
        '@app': {
          templateUrl: 'states/home/home.html',
          controller: 'HomePageCtrl'
        }
      }
    })

    .state('app.events', {
      url: stateUrl.events.landing,
      // The following views could be replaced by
      // templateUrl: templateUrl + '/events.html',
      views: {
        '@app': {
          templateUrl: 'states/events/events.html'
        }
      }
    })

    .state('app.events.event', {
      url: stateUrl.events.event,
      resolve: {
        eventObj: /*@ngInject*/ function ($q, $stateParams, Events) {
          var deferred = $q.defer()

          Events.$loaded().then(function (events) {
            var event = events.$getRecord($stateParams.eventId)

            deferred.resolve(event)
          }).catch(function () {
            deferred.reject('Could not get event oject')
          })

          return deferred.promise
        }
      },
      views: {
        '@app': {
          templateUrl: 'states/events/event/event.html',
          controller: 'EventPageCtrl',
          controllerAs: 'eventPageCtrl'
        }
      }
    })

    .state('app.events.new', {
      url: stateUrl.events.new,
      // parent: 'events',
      views: {
        '@app': {
          templateUrl: 'states/events/new/new.html'
        }
      }
    })

  // For any unmatched url, redirect to homepage
  $urlRouterProvider.otherwise('/')
}
