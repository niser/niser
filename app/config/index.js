import { default as routes } from './routes'
import { default as run } from './run'

/**
 * List Application Angular configs here
 */
angular.module('config').config(routes).run(run)

