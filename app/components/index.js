import { default as EventListDirective } from './event/list'
import { default as EventNewDirective } from './event/new'
import { default as EventThumbnailDirective } from './event/thumbnail'
import { default as EventDetailsDirective } from './event/details'

/**
 * List Application Angular configs here
 */
angular.module('components')

  .directive('eventList', EventListDirective)

  .directive('newEvent', EventNewDirective)

  .directive('eventThumbnail', EventThumbnailDirective)

  .directive('eventDetails', EventDetailsDirective)
