'use strict'
/**
 * Directive of an event in event list

 * @module directives.eventCard
 */
export default /*@ngInject*/ function eventThumbnail (URLS) {
  return {
    scope: {
      event: '='
    },
    restrict: 'E',
    replace: true,
    templateUrl: 'components/event/thumbnail/thumbnail.html',
    controllerAs: 'eventThumbnailCtrl',
    bindToController: true,
    controller: function () {}
  }
}
