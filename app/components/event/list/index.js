/**
 * Directive of event list

 * @module directives.eventList
 */
export default /*@ngInject*/ function eventList (Events, URLS) {
  return {
    restrict: 'E',
    replace: true,
    templateUrl: 'components/event/list/list.html',
    controllerAs: 'eventListCtrl',
    controller: function () {
      this.Events = Events
    }
  }
}
