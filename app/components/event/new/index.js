'use strict'
/**
 * Directive of new event form

 * @module directives.newEvent
 */
export default /*@ngInject*/ function newEvent (Events, URLS) {
  return {
    restrict: 'E',
    replace: true,
    templateUrl: 'components/event/new/new.html',
    controllerAs: 'newEventCtrl',
    bindToController: true,
    controller: function () {
      var ctrl = this

      this.newEvent = function (event) {
        Events
          .$add(event)
          .then(function (eventData) {
            ctrl.resetNewEventForm()
          })
          .catch(function (error) {
            console.error(error)
          // When event is failed to be added
          })
      }

      this.resetNewEventForm = function () {
        ctrl.event = {}
      }
    }
  }
}
