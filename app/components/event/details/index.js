'use strict'
/**
 * Directive of an event details

 * @module directives.eventDetails
 */
export default /*@ngInject*/ function eventDetails (URLS) {
  return {
    scope: {
      eventObj: '='
    },
    restrict: 'E',
    replace: true,
    templateUrl: 'components/event/details/details.html',
    controllerAs: 'eventDetailsCtrl',
    bindToController: true,
    controller: function () {}
  }
}
