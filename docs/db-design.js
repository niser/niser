/**
 *
 * @reference https://www.firebase.com/docs/web/guide/structuring-data.html
 */

users: [{
  userId: string,
  username: string,
  password: string,
  email: string,
  firstname: string,
  lastname: string,
  numberOfGroups: number,
  numberOfEvents: number,
  // /users/:userid/groups/:groupid
  groups: {
    groupId: true
  },
  // /users/:userid/events/:eventid
  events: {
    eventId: true
  }
}]

events: [{
  eventId: string,
  ownerId: string,
  title: string,
  permission: string,
  description: string,
  dateStart: date,
  dateEnd: date,
  location: string,
  numberOfUsers: number,
  numberOfGroups: number,
  // /events/:eventid/users/:userid
  users: {
    userId: true
  },
  // /events/:eventid/groups/:groupid
  groups: {
    groupId: true
  },
}]

groups: [{
  groupId: string,
  name: string,
  numberOfUsers: number,
  numberOfEvents: number,
  // /groups/:groupid/users/:userid
  users: {
    userId: true
  },
  // /groups/:groupid/events/:eventid
  events: {
    eventId: true
  }
}]
