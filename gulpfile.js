'use strict'

var $ = require('gulp-load-plugins')(),
  babelify = require('babelify'),
  browserify = require('browserify'),
  browserSync = require('browser-sync').create(),
  buffer = require('vinyl-buffer'),
  gulp = require('gulp'),
  runSequence = require('run-sequence'),
  source = require('vinyl-source-stream'),
  watchify = require('watchify'),
  // ENV
  env = process.env.NODE_ENV || 'development',
  watch = (env === 'development'),
  compress = !watch

/**
 * List bower depons in index.tpl.html
 *
 * @note This task should run before useref, and modernize and foundation.js are ignored
 */
gulp.task('wiredep', function () {
  var wiredep = require('wiredep').stream
  gulp.src('assets/index.tpl.html')
    .pipe(wiredep({
      exclude: [ /modernizr/, 'bower_components/foundation/js/foundation.js'],
      overrides: {
        "foundation-apps": {
          "main": [
            "dist/js/foundation-apps.js",
            "dist/js/foundation-apps-templates.js"
          ]
        }
      }
    }))
    .pipe($.rename('index.html'))
    .pipe(gulp.dest('assets'))
})

/**
 * Compress bower components into one js file
 */
gulp.task('useref', function () {
  var assets = $.useref.assets()

  return gulp.src('assets/index.html')
    .pipe(assets)
    .pipe($.if('*.js', $.uglify()))
    .pipe($.if('*.css', $.minifyCss()))
    .pipe(assets.restore())
    .pipe($.useref())
    .pipe(gulp.dest('assets'))
})

/**
 * @see  http://mindthecode.com/lets-build-an-angularjs-app-with-browserify-and-gulp
 */
gulp.task('scripts', function () {
  // set up the browserify instance on a task basis
  var bundler, stream

  bundler = browserify({
    entries: './app/app.js'
  })

  if (watch) {
    bundler = watchify(bundler)
  }

  stream = bundler.transform(babelify)
    .bundle()
    .pipe(source('app.js'))
    .pipe(buffer())
    .pipe($.sourcemaps.init({
      loadMaps: true
    }))
    .pipe($.ngAnnotate())

  if (compress) {
    stream = stream.pipe($.uglify())
  }

  return stream.on('error', $.util.log)
    .pipe($.sourcemaps.write('./'))
    .pipe(gulp.dest('./assets/js/'))
})

gulp.task('templates', function () {
  var stream = gulp.src('./app/**/*.html')
    .pipe($.angularTemplatecache({
      module: 'niserApp'
    }))

  if (compress) {
    stream = stream
      .pipe(buffer())
      .pipe($.uglify())
  }

  stream.pipe(gulp.dest('./assets/js'))
})

gulp.task('fonts', function() {

})

gulp.task('icons', function() {
  
})

gulp.task('styles', function () {
  var stream = gulp.src('./assets/scss/**/*.scss')
    .pipe($.sourcemaps.init())

    if (watch) {
      stream = stream.pipe($.sass({errLogToConsole: true}))
    } else{
      stream = stream.pipe($.sass({outputStyle: 'compressed'}))
    }
    
    stream.pipe($.autoprefixer())
      .pipe($.concat('main.css'))
      .pipe($.sourcemaps.write())
      .pipe(gulp.dest('./assets/css'))
})

gulp.task('nodemon', function (cb) {
  var called = false
  return $.nodemon({
    // nodemon our expressjs server
    script: 'server.js',

    // watch core server file(s) that require server restart on change
    watch: ['server.js']
  })
    .on('start', function onStart () {
      // ensure start only got called once
      if (!called) { cb(); }
      called = true
    })
    .on('restart', function onRestart () {
      // reload connected browsers after a slight delay
      setTimeout(function reload () {
        browserSync.reload({
          stream: false
        })
      }, 500)
    })
})

// Ensures the `scripts` task is complete before reloading browsers
gulp.task('watch:scripts', ['scripts'], browserSync.reload)

// Ensures the `templates` task is complete before reloading browsers
gulp.task('watch:templates', ['templates'], browserSync.reload)

// Ensures the `styles` task is complete before reloading browsers
gulp.task('watch:styles', ['styles'], browserSync.reload)

// Watch and initial browserSync
gulp.task('watch', ['nodemon'], function () {
  browserSync.init({
    files: ['./app/**/*.*', './assets/scss/*.*'],
    proxy: 'http://localhost:3000',
    port: 9000
  })
  gulp.watch('./app/**/*.js', ['watch:scripts'])
  gulp.watch('./app/**/*.html', ['watch:templates'])
  gulp.watch('./assets/scss/**/*.scss', ['watch:styles'])
})

// Run it when you are developing
gulp.task('dev', function (cb) {
  cb = cb || function () {}
  runSequence(['wiredep', 'styles', 'scripts', 'templates'], 'watch', cb)
})

// Run it when you want to deploy
gulp.task('build', function (cb) {
  cb = cb || function () {}
  runSequence(['wiredep', 'styles', 'scripts', 'templates'], 'useref', cb)
})

// Run it when you want to test the app in dist folder
gulp.task('server', function (cb) {
  cb = cb || function () {}

  runSequence('build', 'watch', cb)
})
