# Niser Developer Guide
---
The following tools are assumed to be installed in you local machine already:
* node.js
* compass
* git
* bower

### Getting Startted
---
Following [this guide](https://confluence.atlassian.com/pages/viewpage.action?pageId=270827678) to setup ssh connection to bitbucket so that you do not need to enter password each time when you act with our git repos;

Run the commands in your workspace:
`git clone git@bitbucket.org:niser/niser.git` and `cd niser`;

Run the following commands:
`npm install` and `bower install`;

Run `firebase init` and set the `dist` as web directory;

### Gulp tasks
---
`gulp dev` - When you are developing, run this command , it will watch changes and serve up the `src` folder;

`gulp server` - When you want to see what you gonna deploy looks like, or you wanna test on your local server before deploying, please run this command. It will serve up the `dist` folder;

### Coding Style
---
Make sure `gulp lint` does not through warnings;
We use `.jshintrc` as our format style;

### Tips
---
- `src` folder is the development folder; `dist` folder is the deployment folder; that is, when you run `firebase deploy`, all files in `dist` folder will be uploaded to our `firebase server`, then you can see our app via `http://niser.firebaseio.com`;

---



